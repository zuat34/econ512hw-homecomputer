%------------------------------------------------------------
%----------------  ECON 512B HW 2  --------------------------
%----------------  by Suat AKBULUT  -------------------------
%------------------------------------------------------------

%------------------------------------------------------------
%---------------------House Keeping--------------------------
%------------------------------------------------------------
clear all;
close all;
clc;
tic
%------------------------------------------------------------
%---------------------Parameters-----------------------------
%------------------------------------------------------------
delta     = 0.95;                                                          % discount rate
p0        = 0.5;                                                           % curvature of the utility function
rho       = 0.5;
sigma_u   = 0.1;
N         = 5;
%N        = 5:                                                             We can use when N is given to be 5
%------------------------------------------------------------
%------------------- State Space ----------------------------
%------------------------------------------------------------
Lmin      = 0;                                                                   
Linc      = 0.1;                                                               
Lmax      = 100;
L         = (Lmin:Linc:Lmax);                                                  
nL        = size(L,2);

mu        = p0;
ro        = rho;
sigma     = sigma_u; 
[prob, p] = tauchen(N,mu,ro,sigma);
np        = size(prob,2);

%------------------------------------------------------------
%----------------------- VIF --------------------------------
%------------------------------------------------------------
profit  = -Inf*ones(nL,np,nL);

for Lnow = 1:nL;                                                           
    for  pnow = 1:np;                                                      
        for Lnext = 1:Lnow;                                                
% To create current period's profit as function of (L,p,L')
profit(Lnext,pnow,Lnow)= p(pnow)*(L(Lnow)-L(Lnext)) - (0.2)*(L(Lnow)-L(Lnext))^1.5; 
% 1st dim for L', 2nd dim for p, and 3rd dim for L
       end
    end
end

v                        = zeros(np,nL);                                   % Initial value for Value Function
Lpolicy                  = zeros(np,nL);                                   % To store the location of L', this will determine my policy fnc L'=g(p,L)
viter                    = zeros(nL,np,nL);
diff                     = 1;
tol                      = 0.000001;
iter                     = 1

while diff > tol
    
Ev    = prob*v;  
   
    for pnow = 1:np;
        for Lnow = 1:nL;        
            [vnext(pnow,Lnow), Lpolicy(pnow,Lnow)] = max(profit(:,pnow,Lnow) + delta*Ev(pnow,:)',[],1);            
        end
    end
   
    diff   = max(max(abs(vnext-v))); 
    bl     = (delta/(1-delta))*(min(min(vnext-v)));                        % McQueen-Porteus Algorithm (to accelerate the code)
    bh     = (delta/(1-delta))*(max(max(vnext-v)));     
    v      = vnext+(bh+bl)/2;     
    iter   = iter+1
    
end

X =['Iteration is successful. Number of iterations is ', num2str(iter)];
disp(X)

%------------------------------------------------------------
%---------------- Graph of Policy Rule-----------------------
%------------------------------------------------------------

figure
for i=1:N-1
plot(L,L(Lpolicy(i,:)))
hold on   
end
plot(L,L(Lpolicy(N,:)))
line(L,L);
title('Decision Rules')
xlabel('Current Tree Level')
ylabel('Tree Level for the next Period')
%legend('asset choice of employed','asset choice of unemployed')
legend('boxoff')

%------------------------------------------------------------
%--------------- Graph of Value Function---------------------
%------------------------------------------------------------

figure
for i = 1:N-1
plot(L,vnext(i,:));
% here I expected to see Value as function of price, but mistake is really minor
hold on
end
plot(L,vnext(N,:));
title('Value of the Firm')
xlabel('Initial Tree Stock')
ylabel('Expected Discounted Profit')
legend('boxoff')


toc