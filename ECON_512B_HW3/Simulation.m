clc
clear
load('results.mat');
load('ergodic.mat');
% Let s define the probabilities that the firms make sale as follows: 
D=@(p1,p2)[exp(par.nu-p1)/(1+exp(par.nu-p1)+exp(par.nu-p2)),...
           exp(par.nu-p2)/(1+exp(par.nu-p1)+exp(par.nu-p2)),...
           1/(1+exp(par.nu-p1)+exp(par.nu-p2))]';
int_state   = [19,21];
T           = 100000;   %# of simulation
err         = nan(T,1);
draw_now    = [10,20,30];
draw_inx    = 1;
sale_hist   = nan(T,3);
p_hist      = nan(T,2);
hist_state  =[int_state; nan(T-1,2)];
cum_hist    =zeros(par.L); % To store the cumulative history of state
cum_hist(int_state(1),int_state(2)) = cum_hist(int_state(1),int_state(2))+1;


t=1;
U=rand(T,3);% First column to store sale
            % Second column for 1's next period state
            % Third Column for 2's next period state
for t=1:T
   empirical_dist   = cum_hist/sum(sum(cum_hist));
   err(t)           = max(max(abs(empirical_dist-ergodic_dist)));
   p_1_temp         = P_new(hist_state(t,1),hist_state(t,2)); 
   p_2_temp         = P_new(hist_state(t,2),hist_state(t,1));
   p_hist(t,:)      = [p_1_temp,p_2_temp];
   D_temp           = D(p_1_temp,p_2_temp);
   if U(t,1)<= D_temp(1)
       sale = [1,0,0];
   elseif U(t,1)>D_temp(1) && U(t,1)<=D_temp(1)+D_temp(2)
       sale = [0,1,0];
   else
       sale = [0,0,1];
   end
    sale_hist(t,:)=sale;
    pr_1=transP(hist_state(t,1),sale(1),par);
    pr_1_forget=pr_1(2);
    pr_2=transP(hist_state(t,2),sale(2),par);
    pr_2_forget=pr_2(2);

    hist_state(t+1,:) = hist_state(t,:) + sale(1:2)-[U(t,2) < pr_1_forget, U(t,3) < pr_2_forget];
    cum_hist(hist_state(t+1,1),hist_state(t+1,2)) = cum_hist(hist_state(t+1,1),hist_state(t+1,2))+1;
       
end

data=dist_state(cum_hist);
figure(1)
stem3(data(1,:),data(2,:),data(3,:))
xlabel('Firm 2 state')
ylabel('Firm 1 state')
zlabel('Probability')
title(['Emprical process with ' num2str(T) ' periods simulations'])
