function NW = calW2(V_0,W,par)
NW = nan(3,1);
   % outside good is sold
   V_temp_0 = [V_0(W(1),W(2))         , V_0(max(W(1)-1,1),W(2));
               V_0(W(1),max(W(2)-1,1)), V_0(max(W(1)-1,1),max(W(2)-1,1))];
   Pr_temp_0 = kron(transP(W(1),0,par)',transP(W(2),0,par));
   NW(1) = sum(sum(V_temp_0.*Pr_temp_0));
   
   % firm 1 good is sold
   V_temp_1 = [V_0(min(W(1)+1,par.L),W(2))         , V_0(W(1),W(2));
               V_0(min(W(1)+1,par.L),max(W(2)-1,1)), V_0(W(1),max(W(2)-1,1))];
   Pr_temp_1 = kron(transP(W(1),1,par)',transP(W(2),0,par));
   NW(2) = sum(sum(V_temp_1.*Pr_temp_1));
   
   % firm 2 good is sold
   V_temp_2 = [V_0(W(1),min(W(2)+1,par.L)), V_0(max(W(1)-1,1),min(W(2)+1,par.L));
               V_0(W(1),W(2))             , V_0(max(W(1)-1,1),W(2))];
   Pr_temp_2 = kron(transP(W(1),0,par)',transP(W(2),1,par));
   NW(3) = sum(sum(V_temp_2.*Pr_temp_2));
end