function [cumul_emp ,cumul_period] = emp_dist(T,N, P_new,par,W_0 )

D=@(p1,p2)[exp(par.nu-p1)/(1+exp(par.nu-p1)+exp(par.nu-p2)),...
           exp(par.nu-p2)/(1+exp(par.nu-p1)+exp(par.nu-p2)),...
           1/(1+exp(par.nu-p1)+exp(par.nu-p2))]';

cumul_period=zeros(par.L);
cumul_emp=zeros(par.L);
int_state=[W_0(1),W_0(2)];
int_cumul=zeros(par.L);
int_cumul(W_0(1),W_0(2))=int_cumul(W_0(1),W_0(2))+1;

for n=1:N
    hist_state=[int_state; nan(T-1,2)];
    cum_hist=int_cumul;
    U=rand(T,3);
   for t=1:T
   p_1_temp=P_new(hist_state(t,1),hist_state(t,2)); 
   p_2_temp=P_new(hist_state(t,2),hist_state(t,1));
   
   D_temp=D(p_1_temp,p_2_temp);
   if U(t,1)<= D_temp(1)
       sale=[1,0,0];
   elseif U(t,1)>D_temp(1) && U(t,1)<=D_temp(1)+D_temp(2)
       sale=[0,1,0];
   else
       sale=[0,0,1];
   end
 
    pr_1=transP(hist_state(t,1),sale(1),par);
    pr_1_forget=pr_1(2);
    pr_2=transP(hist_state(t,2),sale(2),par);
    pr_2_forget=pr_2(2);
    
    if t==T
        break
    end
    %The break code makes sure that the code do not calculate the unwanted
    %history
    hist_state(t+1,:)=hist_state(t,:)+sale(1:2)-[U(t,2)<pr_1_forget, U(t,3)<pr_2_forget];
    cum_hist(hist_state(t+1,1),hist_state(t+1,2))=cum_hist(hist_state(t+1,1),hist_state(t+1,2))+1;   
   end
   cumul_period(hist_state(end,1),hist_state(end,2))=cumul_period(hist_state(end,1),hist_state(end,2))+1;
   cumul_emp=cumul_emp+cum_hist;
end
%cum_period=cum_period/sum(sum(cum_period));
%cum_emp=cum_emp/sum(sum(cum_emp));
end

