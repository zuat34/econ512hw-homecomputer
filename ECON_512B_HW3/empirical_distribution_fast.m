clear all;
close all;

load('results.mat'); 
load('ergodic.mat');

T=10000; %periods of simulation

chain = markov(markov_equ,T,1); %generate the chain of markov process
cumul_hist = zeros(par.L^2,1);% this matrix stores the cumulative frequency of states

tic
for t=1:T
   cumul_hist(chain(t)) = cumul_hist(chain(t))+1;
end
cum_hist = reshape(cumul_hist,par.L,par.L);
toc

data = dist_state(cum_hist);
figure(1)
stem3(data(1,:),data(2,:),data(3,:))
xlabel('Firm 2 state')
ylabel('Firm 1 state')
zlabel('Probability')
title(['Emprical process with ' num2str(T) ' periods simulations'])