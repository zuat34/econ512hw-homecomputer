clear all;
close all;

% Parameterization
par.L = 30;
par.rho = 0.85;
par.kappa = 10;
par.l = 15;
par.nu = 10;
par.delta = 0.03;
par.beta = 1/1.05;
par.eta = log(par.rho)/log(2);
lambda = 0.9;

% Cost function
c = @(w) par.kappa*w.^par.eta.*(w<par.l) ...
         + par.kappa*par.l^par.eta.*(w>=par.l);

% Demand function
D = @(p1,p2) [exp(par.nu - p1)/(1 + exp(par.nu - p1) + exp(par.nu - p2))];

options = optimoptions('fsolve','Display','off');

V_0 = ones(par.L);        % Initial guess for the value function
P_0 = ones(par.L);        % Initial guess for the policy function

i = 1;                    % Iteration counter
tol = 0.0001;

P_new = nan(par.L);
V_new = nan(par.L);

eps = 1000;
max_i = 10000;

tic
while eps>tol && i<=max_i
    for w_1 = 1:par.L
        for w_2 = 1:par.L
            W = [w_1,w_2];
            W_temp = calW(V_0,W,par);   % Stores W_0, W_1, W_2
            P_op = P_0(w_2,w_1);        % Opponent's pricing decision
            
            % FOC
            p_cal = @(p) 1 - (1 - D(p,P_op))*(p - c(w_1)) ...
                           - par.beta*W_temp(2) ...
                           + par.beta*(D(p,P_op)*W_temp(2) ...
                                     + D(P_op,p)*W_temp(3) ...
                           + (1 - D(p,P_op) - D(P_op,p))*W_temp(1));
            
            p_new = broyden(p_cal,10);  % Solving the FOC
            P_new(w_1,w_2) = p_new;     % New policy function
            
            % Updating the value function
            V_new(w_1,w_2) = D(p_new,P_op)*(p_new - c(w_1)) ...
                             + par.beta*(D(p_new,P_op)*W_temp(2) ...
                             + D(P_op,p_new)*W_temp(3) ...
                             + (1-D(p_new,P_op)-D(P_op,p_new))*W_temp(1));
        end
    end
    
    % Dampening
    V_new = lambda*V_new + (1 - lambda)*V_0; 
    P_new = lambda*P_new + (1 - lambda)*P_0;
    
    eps_1(i) = max(max(abs((V_new - V_0)./(1 + V_new))));
    eps_1(i);
    eps_2(i) = max(max(abs((P_0 - P_new)./(1 + P_new))));
    eps_2(i);
    eps = max(eps_1(i),eps_2(i));
    i = i+1;
    
    V_0 = V_new; 
    P_0 = P_new; 
end
toc

save('results.mat','par','V_new','P_new')

figure(1)
mesh(V_new)
title('Value function','FontSize',14,'FontWeight','bold')
xlabel('Firm 2 state','FontSize',12,'FontWeight','bold')
ylabel('Firm 1 state','FontSize',12,'FontWeight','bold')
zlabel('Policy','FontSize',12,'FontWeight','bold')
matlab2tikz('1_value.tikz');

figure(2)
mesh(P_new)
title('Policy function','FontSize',14,'FontWeight','bold')
xlabel('Firm 2 state','FontSize',12,'FontWeight','bold')
ylabel('Firm 1 state','FontSize',12,'FontWeight','bold')
zlabel('Policy','FontSize',12,'FontWeight','bold')
matlab2tikz('1_policy.tikz');