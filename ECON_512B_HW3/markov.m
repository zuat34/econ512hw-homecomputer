function chain= markov(Pi,T,s_0)
% Pi  : the transition matrix of the markov process
% T   : length of the simulation
% s_0 : inital state
% S   : states
if sum(sum(Pi'))~=length(Pi)
    disp('Warning: At least one row does not sum to one')
end

Tr    = zeros(length(Pi),1);
Trans = nan(size(Pi));
for i = 1:length(Pi)
    Tr = Tr+Pi(:,i);
   Trans(:,i) = Tr;
end
E     = rand(T,1);
chain = [s_0, nan(1,T-1)];
for i = 1:T-1
    next = 5*(Trans(chain(i),:)-E(i)<0)+Trans(chain(i),:)-E(i);
   [~,chain(i+1)]=min(next);  
end
end