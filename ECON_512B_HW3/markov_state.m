clear all;
close all;
clc;

load('results.mat')
Pi = nan(par.L^2);      % Transition probability matrix

i = 1;
tic
for w_1 = 1:par.L
   for w_2    = 1:par.L
      W       = [w_1,w_2];
      Pi_eq_temp = transP_state2(W,par,P_new);
      Pi(i,:) = reshape(Pi_eq_temp,1,par.L^2);
      i       = i+1;
   end
end
toc

markov_equ = Pi; %Markov process of states

[V , D]      = eig(markov_equ'); 
[foo , tp]   = sort(diag(D));
PI           = (V(: , tp(end))/sum(V(: , tp(end))))';
ergodic_dist = reshape(PI,par.L,par.L); %invariant distribution of the markov process, which is ergodic set

save('ergodic.mat','ergodic_dist','markov_equ')

 data_2=dist_state(ergodic_dist);
   figure(1)
   stem3(data_2(1,:),data_2(2,:),data_2(3,:))
 xlabel('Firm 2 state')
   ylabel('Firm 1 state')
   zlabel('Probability')
   title('Stationary Distribution')