clear all;
close all;

load('ergodic.mat');
load('results.mat');

W_0 = [1,1];          % Inital state
T = [10,20,30]';      % Number of periods

init_state = zeros(par.L);
init_state(W_0(1),W_0(2)) = init_state(W_0(1),W_0(2)) + 1;
init_distro = reshape(init_state,par.L^2,1);

for t = 1:length(T)
    distro_period{t} = reshape((markov_equ')^T(t)*init_distro,par.L,par.L);
    data_temp = distro_state(distro_period{t});
    figure(t)
    stem3(data_temp(1,:),data_temp(2,:),data_temp(3,:),':k')
    axis([1 30 1 30 0 0.0520])
    xlabel('Firm 1''s State','FontSize',12,'FontWeight','bold')
    ylabel('Firm 2''s State','FontSize',12,'FontWeight','bold')
    zlabel('Probability','FontSize',12,'FontWeight','bold')
    title(['Distribution of States after ' num2str(T(t)) ' periods'],...
           'FontSize',14,'FontWeight','bold')
end