clear all;
close all;

load('ergodic.mat');
load('results.mat');

W_0 = [1,1];          % Inital state
T = [10,20,30]';      % Number of periods

init_state = zeros(par.L);
init_state(W_0(1),W_0(2)) = init_state(W_0(1),W_0(2)) + 1;
init_distro = reshape(init_state,par.L^2,1);

figure(1)
period_distro_10 = distro_state(reshape((markov_equ')^...
                   10*init_distro,par.L,par.L));
stem3(period_distro_10(1,:),period_distro_10(2,:),...
      period_distro_10(3,:),':k')
    xlabel('Firm 2''s State','FontSize',12,'FontWeight','bold')
    ylabel('Firm 1''s State','FontSize',12,'FontWeight','bold')
    zlabel('Probability','FontSize',12,'FontWeight','bold')
    title(['Distribution of States after 10 periods'],...
           'FontSize',14,'FontWeight','bold')
matlab2tikz('2_10_periods.tikz');

figure(2)
period_distro_20 = distro_state(reshape((markov_equ')^...
                   20*init_distro,par.L,par.L));
stem3(period_distro_20(1,:),period_distro_20(2,:),...
      period_distro_20(3,:),':k')
    xlabel('Firm 2''s State','FontSize',12,'FontWeight','bold')
    ylabel('Firm 1''s State','FontSize',12,'FontWeight','bold')
    zlabel('Probability','FontSize',12,'FontWeight','bold')
    title(['Distribution of States after 20 periods'],...
           'FontSize',14,'FontWeight','bold')
matlab2tikz('2_20_periods.tikz');

figure(3)
period_distro_30 = distro_state(reshape((markov_equ')^...
                   30*init_distro,par.L,par.L));
stem3(period_distro_30(1,:),period_distro_30(2,:),...
      period_distro_30(3,:),':k')
    xlabel('Firm 2''s State','FontSize',12,'FontWeight','bold')
    ylabel('Firm 1''s State','FontSize',12,'FontWeight','bold')
    zlabel('Probability','FontSize',12,'FontWeight','bold')
    title(['Distribution of States after 30 periods'],...
           'FontSize',14,'FontWeight','bold')
matlab2tikz('2_30_periods.tikz');