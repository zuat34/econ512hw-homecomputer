function [pr] = transP(w,q,par)
if w == par.L && q==1
    pr_remember = 0;
    pr_forget = 1;
    
elseif w==1 && q==0
    pr_remember = 1;
    pr_forget = 0;

else
    pr_remember = (1 - par.delta)^w;
    pr_forget = 1 - (1 - par.delta)^w;
end

pr = [pr_remember;pr_forget];
end