function  Pi_state=transP_state2(W,par,P_new)
Pi_state=zeros(par.L);
NW=nan(3,1);
w_1=W(1);
w_2=W(2);
p1=P_new(W(1),W(2));
p2=P_new(W(2),W(1));
D=[1/(1+exp(par.nu-p1)+exp(par.nu-p2)),...
   exp(par.nu-p1)/(1+exp(par.nu-p1)+exp(par.nu-p2)),...
   exp(par.nu-p2)/(1+exp(par.nu-p1)+exp(par.nu-p2))]';

%outside good is sold
   W_next_0={[w_1,w_2]          , [w_1,max(w_2-1,1)];
             [max(w_1-1,1),w_2] , [max(w_1-1,1),max(w_2-1,1)]};
 
   Pr_temp_0=kron(transP(W(2),0,par)',transP(W(1),0,par));
   for i=1:2
      for j=1:2
         Pi_state(W_next_0{i,j}(1),W_next_0{i,j}(2))= Pi_state(W_next_0{i,j}(1),W_next_0{i,j}(2))...
                                        +D(1)*Pr_temp_0(i,j);
      end
   end
%firm 1 good is sold   
   W_next_1={[min(w_1+1,par.L),w_2]     , [w_1,max(w_2-1,1)];
             [min(w_1+1,par.L),w_2]     , [w_1,max(w_2-1,1)]};
 
   Pr_temp_1=kron(transP(W(2),0,par)',transP(W(1),1,par));
   for i=1:2
      for j=1:2
         Pi_state(W_next_1{i,j}(1),W_next_1{i,j}(2))= Pi_state(W_next_1{i,j}(1),W_next_1{i,j}(2))...
                                        +D(2)*Pr_temp_1(i,j);
      end
   end
%firm 2 good is sold   
   W_next_2={[w_1,min(w_2+1,par.L)]              , [w_1,w_2];
             [max(w_1-1,1),min(w_2+1,par.L)]     , [max(w_1-1,1),w_2]};
 
   Pr_temp_2=kron(transP(W(2),1,par)',transP(W(1),0,par));
   for i=1:2
      for j=1:2
         Pi_state(W_next_2{i,j}(1),W_next_2{i,j}(2))= Pi_state(W_next_2{i,j}(1),W_next_2{i,j}(2))...
                                        +D(3)*Pr_temp_2(i,j);
      end
   end   
end