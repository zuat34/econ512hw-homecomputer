%% ECON 512A HOMEWORK 2
% SUAT AKBULUT sqa5456@psu.edu
% State College, PA
%------------------------------------------------------
%--------------------House Keeping---------------------
%------------------------------------------------------
% Comment from TA: you have got everithing right, but you used singular FOC
% that caused all the problems. I think it is what Charlie wanted you to do
% though. Take a look at answer key, I'm using modified FOC there that does
% not have a problem with q(i) approaching zero. 
close all;
clear;
clc;


%% ------------------------------------------------------
%-----------------------QUESTION 1-----------------------
%--------------------------------------------------------

[q_A q_B q_C q_0]= q1(1,1,1,-1,-1,-1)  

%% ------------------------------------------------------
%-----------------------QUESTION 2-----------------------
%--------------------------------------------------------


V       = [-1 -1 -1];  
optim_p = @(p) q2(V(1),V(2),V(3),p);  

%(a)
init_guess= [1;1;1]
tol=1e-10;                     

optset('broyden','tol',tol);
tic;
sol_boryden = broyden(optim_p,init_guess)'
time_broyden=toc;
disp('Broyden runtime')
disp(time_broyden)

%(b)
init_guess= [0;0;0]
tol=1e-10;                     

optset('broyden','tol',tol);
tic;
sol_boryden = broyden(optim_p,init_guess)'
time_broyden=toc;
disp('Broyden runtime')
disp(time_broyden)

%(c)
init_guess= [0;1;2]
tol=1e-10;                     

optset('broyden','tol',tol);
tic;
sol_boryden = broyden(optim_p,init_guess)'
time_broyden=toc;
disp('Broyden runtime')
disp(time_broyden)

%(d)
init_guess= [3;2;1]
tol=1e-10;                      

optset('broyden','tol',tol);
tic;
sol_boryden = broyden(optim_p,init_guess)'
time_broyden=toc;
disp('Broyden runtime')
disp(time_broyden)


%% ------------------------------------------------------
%-----------------------QUESTION 3-----------------------
%--------------------------------------------------------

p0 = init_guess;
err = 1;
i = 1;
tic;
while abs(err)>=tol
   [x,y] = q3(-1,-1,-1,p0);
   p0 = x;
   err = max(y);
   i = i+1;
end
time_gauss_seidel=toc;
sol_gauss_seidel = p0'
disp('Gauss-Seidel runtime')
disp(time_gauss_seidel)


%% ------------------------------------------------------
%-----------------------QUESTION 4-----------------------
%--------------------------------------------------------

p0 = init_guess;
err = 1;
i = 1;
tic;
while abs(err)>=tol
   x = q4(-1,-1,-1,p0);
   err = max(abs(x-p0));
   p0 = x;
   i = i+1;
end
time_iterate=toc;
sol_iterate=p0'
disp('Iteration method runtime')
disp(time_iterate)


%% ------------------------------------------------------
%-----------------------QUESTION 5-----------------------
%--------------------------------------------------------

v_C        = -4:1;
init_guess = [1;1;1];
nl         = length(v_C);
pA         = zeros(nl,1);
pC         = zeros(nl,1);
for i = 1:nl
    optim_p_temp  = @(p) q2(-1,-1,v_C(i),p);     
    sol_temp      = broyden(optim_p_temp,init_guess);
    
    pA(i)         = sol_temp(1);
    pB(i)         = sol_temp(3);
end

figure
plot(v_C,pA,v_C,pB);
legend('p_A','p_C')

