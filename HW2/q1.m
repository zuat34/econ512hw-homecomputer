function [q_A, q_B, q_C, q_0]= q1(p_A, p_B, p_C, v_A, v_B, v_C)

q_A = exp(v_A-p_A)/(1+exp(v_A-p_A)+exp(v_B-p_B)+exp(v_C-p_C)); 

q_B = exp(v_B-p_B)/(1+exp(v_A-p_A)+exp(v_B-p_B)+exp(v_C-p_C));  

q_C = exp(v_C-p_C)/(1+exp(v_A-p_A)+exp(v_B-p_B)+exp(v_C-p_C)); 

q_0 = 1/(1+exp(v_A-p_A)+exp(v_B-p_B)+exp(v_C-p_C));            

end