function X = q2(v_A, v_B, v_C, p)

q_A = exp(v_A-p(1))/(1+exp(v_A-p(1))+exp(v_B-p(2))+exp(v_C-p(3))); 

q_B = exp(v_B-p(2))/(1+exp(v_A-p(1))+exp(v_B-p(2))+exp(v_C-p(3))); 

q_C = exp(v_C-p(3))/(1+exp(v_A-p(1))+exp(v_B-p(2))+exp(v_C-p(3))); 

X = [q_A-p(1)*q_A*(1-q_A); q_B-p(2)*q_B*(1-q_B); q_C-p(3)*q_C*(1-q_C)];                
end