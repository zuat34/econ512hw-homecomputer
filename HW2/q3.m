function [X,Y] = q3(v_A,v_B,v_C,p)

options = optimset('Display','off');

q_A = @(p_A) exp(v_A-p_A)/(1+exp(v_A-p_A)+exp(v_B-p(2))+exp(v_C-p(3)));

fun_A=@(p_A) q_A(p_A)-p_A*q_A(p_A)*(1-q_A(p_A));

root1 = fsolve(fun_A,p(1),options);  

q_B = @(p_B) exp(v_B-p_B)/(1+exp(v_A-root1)+exp(v_B-p_B)+exp(v_C-p(3)));

fun_B=@(p_B)q_B(p_B)-p_B*q_B(p_B)*(1-q_B(p_B));

root2=fsolve(fun_B,p(2),options);

q_C = @(p_C) exp(v_C-p_C)/(1+exp(v_A-root1)+exp(v_B-root2)+exp(v_C-p_C));

fun_C=@(p_C)q_C(p_C)-p_C*q_C(p_C)*(1-q_C(p_C));

root3=fsolve(fun_C,p(3),options);


X=[root1;
   root2;
   root3];

Y=abs(p-X); 
end