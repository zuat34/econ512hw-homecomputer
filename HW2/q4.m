function X = q4(v_A,v_B,v_C,p)

q_A = exp(v_A-p(1))/(1+exp(v_A-p(1))+exp(v_B-p(2))+exp(v_C-p(3)));
q_B = exp(v_B-p(2))/(1+exp(v_A-p(1))+exp(v_B-p(2))+exp(v_C-p(3)));
q_C = exp(v_C-p(3))/(1+exp(v_A-p(1))+exp(v_B-p(2))+exp(v_C-p(3)));

X=[inv(1-q_A); inv(1-q_B); inv(1-q_C)];

q_A2 = exp(v_A-X(1))/(1+exp(v_A-X(1))+exp(v_B-X(2))+exp(v_C-X(3)));
q_B2 = exp(v_B-X(2))/(1+exp(v_A-X(1))+exp(v_B-X(2))+exp(v_C-X(3)));
q_C2 = exp(v_C-X(3))/(1+exp(v_A-X(1))+exp(v_B-X(2))+exp(v_C-X(3)));

end