function beta_hat = BHHH(X,y,beta_0,tol)

err = 10^10;
beta_init = beta_0;

while err>=tol
   beta_temp = BHHH_update(X,y,beta_init);
   err = max(abs(beta_temp-beta_init));
   beta_init = beta_temp;
end

beta_hat=beta_init;
end