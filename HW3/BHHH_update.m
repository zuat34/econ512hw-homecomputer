function beta_next = BHHH_update(X,y,beta)

[n,k] = size(X);
grad_store = nan(k,n);

for i=1:n
    grad_store(:,i) = grad(X(i,:),y(i),beta);   
end

G = sum(grad_store')';
J = zeros(k);

for i = 1:n
   J = grad_store(:,i)*grad_store(:,i)' + J; 
end

beta_next = beta + J\G;
    
    function g = grad(x_loc,y_loc,beta)
        g = -exp(x_loc*beta)*x_loc' + y_loc*x_loc';
    end
end