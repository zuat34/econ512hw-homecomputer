%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%         ECON 512A - HW3        %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%% Suat Akbulut - sqa5456@psu.edu %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%       State College, PA        %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%       House Keeping        %%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all
clear
clc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Loading data
load('hw3.mat')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Question 1
nX          = size(X,2);        % Dimension of Beta
beta0       = zeros(nX,1);      % Initial guess for fminunc

% Without supplying the derivative
opt_wo      = optimoptions('fminunc','Display','iter','GradObj','off','Algorithm','quasi-newton','HessUpdate','bfgs');
beta_wo     = fminunc(@(b) log_lhd(X,y,b), beta0, opt_wo);

% Supplying the derivative
opt_der     = optimoptions('fminunc','Display','iter','GradObj','on','Algorithm','quasi-newton','HessUpdate','bfgs');
beta_der    = fminunc(@(b) log_lhd(X,y,b), beta0, opt_der);


% Using the N-M Method
optset('neldmead','maxit',2000);    
optset('neldmead','showiters',1);
beta_NM     = neldmead(@(b) -log_lhd(X,y,b), beta0);

% Using the BHHH Method
tol = 1e-10;
beta_BHHH   = BHHH(X,y,beta0,tol);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Question 2

% Eigenvalues for the Hessian approximation for the BHHH MLE method:
% For the initial Hessian:
eigval_init = eig(hess_Garima(X,beta0));
% For the Hessian at the estimated parameters.
eigval_est  = eig(hess_Garima(X,beta_BHHH));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Question 3

optionsq3 = optimoptions('lsqnonlin','Display','iter');
beta_NLLS= lsqnonlin(@(b) y-exp(X*b),beta0,[],[],optionsq3);



