function hessian = hess_Garima(X,b)

% Similar to the gradient, since this is just taking the derivate of the
% gradiaent, I made us of Garima's Hessian function again.
    [m,n] = size(X);
hess_temp = zeros(n,n);

for i=1:m
    hess_temp = Hess_temp(X(i,:),b) + hess_temp;
end

    hessian = hess_temp;

    function H_temp = Hess_temp(x_loc,b)
        H_temp = exp(x_loc*b)*(x_loc')*(x_loc);
    end
end