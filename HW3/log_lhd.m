function [log_f, dlog_f] = log_lhd(X,y,beta_0);

[n,k]=size(X);
grad_store=nan(k,n);

log_f = -sum(-exp(X*beta_0) + y.*(X*beta_0) - log(factorial(y)));

for i=1:n;
    grad_store(:,i) = gra(X(i,:),y(i),beta_0); 
end

dlog_f = -sum(grad_store')';

    function g = gra(x_loc,y_loc,beta);
        g = -exp(x_loc*beta)*x_loc' + y_loc*x_loc'; 
    end
end