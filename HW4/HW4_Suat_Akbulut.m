%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%         ECON 512A - HW4        %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%% Suat Akbulut - sqa5456@psu.edu %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%       State College, PA        %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%       House Keeping        %%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all
clear
clc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Loading data
load('hw4data.mat')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Our Initial Guess (same for both parts a and b)
init_guess = ones(6,1);
% Parameters given in the question
% g, beta_0, u_0, sigma_b, sigma_u, sigma_bu (the order)
ss  =[0, 0.1, 0, 1, 0, 0];

% with 5 nodes
nodes = 5;
% Our objective function with 5 nodes 
my_fun = @(s) lhd(s(1),s(2),s(3),s(4),s(5),s(6),data, nodes);
% likelihood function for given parameters
my_fun(ss)
% Our MLE estimator (which is the minimizer of the above equation)
MLE_a = fminunc(my_fun,init_guess)
% The value of the minimized Likelihood function
my_fun(MLE_a)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% with 5 nodes
nodes = 10; 
% Our objective function with 10 nodes 
my_fun = @(s) lhd(s(1),s(2),s(3),s(4),s(5),s(6),data, nodes);
% likelihood function for given parameters
my_fun(ss)
% Our MLE estimator (which is the minimizer of the above equation)
MLE_b = fminunc(my_fun,init_guess)
% The value of the minimized Likelihood function
my_fun(MLE_b)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% I could not figuer out how to do the Monte Carlo parts