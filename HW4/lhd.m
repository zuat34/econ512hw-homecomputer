function [LHD,LHD_i] = lhd(g,beta_0,u_0,sigma_b,sigma_u,sigma_bu,data,M)

LHD_temp = nan(data.N,1);
mu = [beta_0; u_0];
S =  [sigma_b, sigma_bu;
      sigma_bu, sigma_u];

[b,u] = qnwnorm([M M],mu,S); % bivariate normal on the quadrature points

for k=1:data.N
    x = data.X(:,k);
    y = data.Y(:,k);
    z = data.Z(:,k);
    
    LHD_temp(k) = u'*indiv_lhd(data.X(:,k),data.Y(:,k),data.Z(:,k),...
                               b(:,1), g, b(:,2));
                           
    % Comment from TA: why are you multiplying by u'? it should be part of
    % individual likelihood
end

LHD_i = LHD_temp;
LHD = -sum(log(LHD_i));

% Indiviual likelihood function for each observation
    function L_i = indiv_lhd(x,y,z,beta,gamma,u)
        F = @(e) (1+exp(-e)).^(-1);
        arg = kron(beta',x) + kron(gamma*ones(size(beta')),z) + ...
              kron(u',ones(size(x)));
        L_i = prod(F(arg).^(kron(ones(size(beta')), y)).* ...
                   (1-F(arg)).^(1 - kron(ones(size(beta')),y)))';
    end
end